import 'package:coursesApp/api/api_base_helper.dart';
import 'package:coursesApp/models/movie.dart';
import 'package:coursesApp/responses/movie_response.dart';
import 'package:coursesApp/utils/contants.dart';

class MovieRepository {
  ApiBaseHelper _helper = ApiBaseHelper();

  Future<List<Movie>> fetchMovieList() async {
    final response = await _helper.get(Constants.apiBaseUrl);
    return MovieResponse.fromJson(response).results;
  }
}
